# RotaryPi

Python library to read the rotary dial of an old telephone with a Raspberry Pi.

Targets Python `>3.9` on a Raspberry Pi.

This library was developed using a **Tfg 3-39.205** desktop telephone from 1974, but it should work with a wide variety of rotary dials since the basic principle is pretty strait forward and probably very similar on various models. But I only have this particular model, so I've only tested it on that particular one.

## Documentation

Read the full documentation on [readthedocs](https://rotarypi.readthedocs.io/en/latest/).
