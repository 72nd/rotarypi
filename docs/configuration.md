# Configuration

The `RotaryReader` object can be configured via its constructor.

## Pin-out

To change the pin numbers, pass a `DialPinout` object to the constructor:

```python
from queue import Queue
from rotarypi import RotaryReader, DialPinout

q = Queue()
pins = DialPinout(
    counter_pin=5,
    dial_pin=6,
    handset_pin=8
)
reader = RotaryReader(queue=q, pinout=pins)
```

## Configuration

To change some configuration options, pass a `DialConfiguration` object to the constructor:

```python
import logging
from queue import Queue
from rotarypi import RotaryReader, DialConfiguration

q = Queue()
configuration = DialConfiguration(
    loglevel=logging.DEBUG,
    counter_debounce=100,
    dial_debounce=120,
    handset_debounce=120
)
reader = RotaryReader(queue=q, config=configuration)
```

### Logging Level

The `RotaryReader` contains a logger, writing to STDOUT on various events, to debug the library. Only the levels `INFO` and `DEBUG` are used, so from `WARNING` upwards, the logger remains silent. Defaults to `WARNING`.

### Counter Debounce

Debounce value of the counter pin in ms. Allows to adjust the library to different telephones. If the read number is often higher than the one dialed, this value should probably be increased. If the read number is often lower than the dialed one, this value should probably be decreased. Defaults to `80`.

### Dial Debounce

Debounce value of the dial pin in ms. Allows to adjust the library to different telephones. Defaults to `100`.

### Handset Debounce

Debounce value of the handset pin in ms. Allows to adjust the library to different telephones. Defaults to `100`.
