# RotaryPi

Python library to read the rotary dial of an old telephone with a Raspberry Pi

## Compatibility

This library was developed using a **Tfg 3-39.205** desktop telephone from 1974, but it should work with a wide variety of rotary dials since the basic principle is pretty strait forward and probably very similar on various models. But I only have this particular model, so I've only tested it on that particular one.

Targets Python `>=3.9` on a Raspberry Pi.

## Hardware

This library assumes that the rotary dial is essentially made up of two switches and an additional switch is used to detect if the handset is picked up or hung up:

- The **dial switch** is normally open. It closes when the dial is rotated clockwise to dial a number, stays closed when the rotation direction changes and opens when the counter clockwise rotation stops.
- The **counter switch** is normally closed. On the counter clockwise rotation, the switch opens briefly for every number it passes on the rotation, forming `n` impulses on the counter clockwise rotation when the number `n` is dialed (and 10 impulses for the number 0)
- The **handset switch** is open, when the handset is hung up. It closes when the handset is picked up.

The library uses internal pull-up resistors on on the Pi. One side of each switch is connected to GND, the other side of each switch is connected to GPIO pins of the Pi:

- The **dial pin** is pulled down whenever the dialing process starts. By default, it is connected to `GPIO 26`.
- The **counter pin** is pulled up briefly whenever the counter clockwise rotation passes a number. By default, it is connected to `GPIO 19`.
- The **handset pin** is high when the handset is hung up and low when the handset is picked up. By default, it is connected to `GPIO 13`.

To change the pin numbers, check the configuration page.

## Installation

Install the package using pip:

```bash
pip3 install rotarypi
```

### Dependencies

pip will install the package's only dependency, `RPi.GPIO`. Obviously, this only works properly on a Pi.

