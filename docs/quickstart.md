# Quick Start

The library is used by creating a `RotaryReader` object. This object requires a `queue.Queue` as a parameter. The `.start()` method starts the multithreaded reading process. Whenever the handset is moved or a number is dialed, an instance of the dataclass `DialEvent` is pushed onto the queue.

This example listens for five events, stops and cleans up:

```python
from queue import Queue
from rotarypi import RotaryReader, DialEvent

q: Queue[DialEvent] = Queue()
reader = RotaryReader(queue=q)

reader.start()

for i in range(5):
    val: int = q.get()
    print(val)
    
reader.stop()
reader.cleanup()
```

`DialEvent` is a frozen dataclass containing information about the event that just occured:

```python
@dataclass(frozen=True)
class DialEvent:
    type: EventType
    data: Union[HandsetState, int]
```

The `type` field contains an enumeration member denoting whether a number was dialed or the handset was moved. The enumeration has two members:

```python
class EventType(Enum):
    DIAL_EVENT = 0
    HANDSET_EVENT = 1
```

The `data` field contains the number dialed as an `int` if a number was dialed or the state of the handset, represented by the following enumeration:

```python
class HandsetState(Enum):
    HUNG_UP = 0
    PICKED_UP = 1
```
