# Testing

The library is too small and to hardware-dependent to include useful unit tests. While technically not a unit test, you can run a basic program printing all dialed numbers to STDOUT to test the hardware setup:

```python3
python3 -m unittest discover
```
