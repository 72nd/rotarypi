import logging
import unittest
from queue import Queue

from rotarypi import DialPinout, DialConfiguration, RotaryReader


class TestEndlessPrint(unittest.TestCase):

    def test_endless(self):
        queue: Queue[int] = Queue()
        pinout: DialPinout = DialPinout()
        config: DialConfiguration = DialConfiguration(loglevel=logging.DEBUG)
        reader: RotaryReader = RotaryReader(queue, pinout, config)
        reader.start()
        while True:
            dialed: int = queue.get()
            print(dialed)
